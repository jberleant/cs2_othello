#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#define WEIGHTS_FILE "./weights"
#define NET_INPUT_SIZE 128
#define NET_HIDDEN1_SIZE 64
#define NET_HIDDEN2_SIZE 64
#define NET_OUTPUT_SIZE 64

#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <fstream>
#include <cmath>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
    void loadWeights();
    void loadInputs(Board *b);
    void propagateNodes();
    std::list<Move> getBestMoves(Board *b, Side side, unsigned int numMoves);
    Move *decisionTree(std::vector<Move>& moves, std::vector<Board*>& boards, Side side, time_t startTime, int millisToWork); 

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);

private:
    Board *board;
    Side side;

    double *net_inputs;
    double **net_weights_in;
    double *net_hidden1;
    double **net_weights_hidden;
    double *net_hidden2;
    double **net_weights_out;
    double *net_outputs;

};

#endif
