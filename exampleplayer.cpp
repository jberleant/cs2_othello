#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    board = new Board();
    this->side = side;

    net_inputs = new double[NET_INPUT_SIZE];
    net_weights_in = new double*[NET_INPUT_SIZE];
    for (int i=0; i<NET_INPUT_SIZE; i++) net_weights_in[i] = new double[NET_HIDDEN1_SIZE];
    net_hidden1 = new double[NET_HIDDEN1_SIZE];
    net_weights_hidden = new double*[NET_HIDDEN1_SIZE];
    for (int i=0; i<NET_HIDDEN1_SIZE; i++) net_weights_hidden[i] = new double[NET_HIDDEN2_SIZE];
    net_hidden2 = new double[NET_HIDDEN2_SIZE];
    net_weights_out = new double*[NET_HIDDEN2_SIZE];
    for (int i=0; i<NET_HIDDEN2_SIZE; i++) net_weights_out[i] = new double[NET_OUTPUT_SIZE];
    net_outputs = new double[NET_OUTPUT_SIZE];

    loadWeights();
}

void ExamplePlayer::loadWeights() {
    ifstream in;

    in.open(WEIGHTS_FILE);
    for (int i=0; i<NET_INPUT_SIZE; i++) {
        for (int j=0; j<NET_HIDDEN1_SIZE; j++) {
            in >> net_weights_in[i][j];
        }
    }
    for (int i=0; i<NET_HIDDEN1_SIZE; i++) {
        for (int j=0; j<NET_HIDDEN2_SIZE; j++) {
            in >> net_weights_hidden[i][j];
        }
    }
    for (int i=0; i<NET_HIDDEN2_SIZE; i++) {
        for (int j=0; j<NET_OUTPUT_SIZE; j++) {
            in >> net_weights_out[i][j];
        }
    }
        
    in.close();
}

void ExamplePlayer::loadInputs(Board *b) {
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
            net_inputs[j*8+i] = b->get(side, i, j);
        }
    }
}

void ExamplePlayer::propagateNodes() {
    for (int i=0; i<NET_HIDDEN1_SIZE; i++) {
        double val = 0;
        for (int j=0; j<NET_INPUT_SIZE; j++) {
            val += net_weights_in[j][i]*net_inputs[j];
        }
        net_hidden1[i] = 1/(1.0+exp(-val));
    }

    for (int i=0; i<NET_HIDDEN2_SIZE; i++) {
        double val = 0;
        for (int j=0; j<NET_HIDDEN1_SIZE; j++) {
            val += net_weights_hidden[j][i]*net_hidden1[j];
        }
        net_hidden2[i] = 1/(1.0+exp(-val));
    }

    for (int i=0; i<NET_OUTPUT_SIZE; i++) {
        double val = 0;
        for (int j=0; j<NET_HIDDEN2_SIZE; j++) {
            val += net_weights_out[j][i]*net_hidden2[j];
        }
        net_outputs[i] = 1/(1.0+exp(-val));
    }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete board;
    delete[] net_inputs;
    delete[] net_hidden1;
    delete[] net_hidden2;
    delete[] net_outputs;
    for (int i=0; i<NET_INPUT_SIZE; i++) delete[] net_weights_in[i];
    delete[] net_weights_in;
    for (int i=0; i<NET_HIDDEN1_SIZE; i++) delete[] net_weights_hidden[i];
    delete[] net_weights_hidden;
    for (int i=0; i<NET_HIDDEN2_SIZE; i++) delete[] net_weights_out[i];
    delete[] net_weights_out;
    
}

std::list<Move> ExamplePlayer::getBestMoves(Board *b, Side side, unsigned int numMoves) {
    std::vector<Move> moves = b->getValidMoves(side); 
    std::list<Move> bestMoves;

    if (moves.size() <= numMoves) {
        for (unsigned int i=0; i<moves.size(); i++) {
            bestMoves.push_back(moves[i]);
        }
        return bestMoves;
    }

    loadInputs(b);
    propagateNodes();
    for (unsigned int i=0; i<moves.size(); i++) {
        double score = net_outputs[moves[i].getX() + moves[i].getY()*8];
        std::list<Move>::reverse_iterator it;
        for (it = bestMoves.rbegin(); it != bestMoves.rend(); it++) {
            if (net_outputs[it->getX() + it->getY()*8] > score)
                break;
        }
        if (bestMoves.size() < numMoves || it != bestMoves.rbegin()) {
            bestMoves.insert(it.base(), moves[i]);
        }
        if (bestMoves.size() > numMoves) {
            bestMoves.pop_back();
        }
    }

    return bestMoves;
    
}
    


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    board->doMove(opponentsMove, (side==WHITE?BLACK:WHITE));

    std::vector<Board*> boards;
    std::vector<Move> moves;


    std::list<Move> bestMoves = getBestMoves(board,side,5);
    if (bestMoves.size() == 0) return NULL;

    for (std::list<Move>::iterator it=bestMoves.begin(); it!=bestMoves.end(); it++) {
        Board *newBoard = board->copy();
        newBoard->doMove(&*it, side);
        boards.push_back(newBoard);
        moves.push_back(*it);
    }
    
    Move *move = decisionTree(moves, boards, side, time(NULL), msLeft*2/5/100); 
    board->doMove(move, side);
    return move;
        
}
Move *ExamplePlayer::decisionTree(std::vector<Move>& moves, std::vector<Board*>& boards, Side side, const time_t startTime, int millisToWork) { 
    time_t now;
    time(&now);
    if (difftime(now, startTime)+boards.size()/1000 >= millisToWork/1000.0) {
        int bestBoard = 0;
        int score = -64; 
        for (unsigned int i=0; i<boards.size(); i++) {
            if (boards[i]->score(this->side) > score) {
                bestBoard = i;
                score = boards[i]->score(this->side);
            }
        }
        return new Move(moves[bestBoard].getX(), moves[bestBoard].getY()); 
    }

    std::vector<Board*> nextBoards;
    std::vector<Move> boardMoves;
    bool treeFinished = true;
    for (unsigned int i=0; i<boards.size(); i++) {
        std::list<Move> nextMoves = getBestMoves(boards[i], side, 3);
        if (nextMoves.size() == 0) {
            nextBoards.push_back(boards[i]);
            boardMoves.push_back(moves[i]);
            continue;
        }
       
        treeFinished = false;
        for (std::list<Move>::iterator it=nextMoves.begin(); it!=nextMoves.end(); it++) {
            Board *newBoard = boards[i]->copy();
            newBoard->doMove(&*it, side);
            nextBoards.push_back(newBoard);
            boardMoves.push_back(moves[i]);
        }
    }
    return decisionTree(boardMoves, nextBoards, side==WHITE?BLACK:WHITE, startTime, (treeFinished?0:millisToWork));
}
